<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/template.css') }}">
    <title>Hello, world!</title>
  </head>
  <body>
    <header class="header">
        <div class="divHeader">
            <div class="logo"></div>
            <div class="menu">
                <a href="{{ route('index') }}" class="link">Inicio</a>
                <a href="{{ route('create') }}" class="link">Crear Movie</a>
            </div>
        </div>
    </header>
        @yield('content')
    <footer class="footer">
        <div class="divFooter">
          © 2020 Copyright: movieslc.com.ar
        </div>
    </footer>
  </body>
</html>