<link rel="stylesheet" href="{{ asset('css/index.css') }}">
@extends('template')

@section('content')
    
    <section class="sectionMovies">
        <div class="divSearch">
            <h2 class="titleSearch">Buscador Movies-LC</h2>
            <input class="seacrh" type="text">
        </div>
        <div class="divMovie">
        @foreach($movies as $item)
            <div class="movie">
                <div class="optionMovie"></div>
                <img class="imgMovie" src="{{ asset('image/'.$item->image) }}"></img>
                <div class="nameMovie">
                    {{$item->name}}
                </div>
            </div>
        @endforeach
        </div>
    </section>
@endsection
