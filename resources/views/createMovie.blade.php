<link rel="stylesheet" href="{{ asset('css/createMovie.css') }}">
@extends('template')

@section('content')
    <section class="section">
        <div class="subTitle">Craer Pelicula</div>
        <form action="{{ route('postMovie') }}" method="post" class="formCreate" enctype="multipart/form-data">
        @csrf
            <div class="divForm">
                <label class="labelForm">Nombre</label>
                <input name="name" class="inputForm">
            </div>

            <div class="divForm">
                <label class="labelForm">Description</label>
                <textarea name="description" class="inputForm textarea"></textarea>
            </div>

            <div class="divForm">
                <label class="labelForm">Imagen</label>
                <input name="image" type="file" class="file">
            </div>
            <div>
                <button class="buttonCreate">Crear</button>
            </div>
            
        </form>
    </section>
@endsection
