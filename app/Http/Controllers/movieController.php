<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\movie;

class movieController extends Controller
{
    public function index(){
        $movies = movie::all();
        
        return view('index',compact('movies'));
    }

    public function createMovie(){
        
        return view('createMovie');
    }

    public function postMovie(Request $request){  
        $filename = $request->file('image');
        $image = time()."-".$filename->getClientOriginalName();
        $filename->move('image',$image);
        $filename = $request->file('image');
        $newMovie = new movie;
        $newMovie->name = $request->name; 
        $newMovie->description = $request->description;
        $newMovie->image = $image;
        $newMovie->video = 'video'; 
        $newMovie->save();

        return $image;
    }
}
